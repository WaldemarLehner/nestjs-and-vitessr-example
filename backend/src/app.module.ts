import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import VueSSRMiddleware from './vue-ssr.middleware';

@Module({
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure( consumer : MiddlewareConsumer )
  {

    consumer.apply(VueSSRMiddleware)
      .exclude("api/(.*)") // Do not apply for any /api/... route
      .forRoutes("*");
  }
}

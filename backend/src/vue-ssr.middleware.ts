import { Injectable, NestMiddleware } from "@nestjs/common";
import { Request, Response } from "express";
import {readFileSync} from "fs"
import {resolve} from "path";

import {render as renderVue} from "../../frontend/dist/server/entry-server.js";

@Injectable()
export default class VueSSRMiddleware implements NestMiddleware
{
    private htmlTemplate: string = undefined;
    private manifest: Record<string, unknown>;

    constructor()
    {
        this.htmlTemplate = readFileSync(
            resolve(__dirname, "../../frontend/dist/client/index.html"), "utf-8"
        )

        const manifestData = readFileSync(
            resolve(__dirname, "../../frontend/dist/client/ssr-manifest.json"), "utf-8"
        );
        this.manifest = JSON.parse(manifestData);

    }

    async use(req: Request, res: Response)
    {
        const url = req.originalUrl;
        console.debug("Vue SSR Middleware triggered @ "+url)
        
        
        const [appHtml, preloadLinks]: [string, any] = await renderVue(url, this.manifest)
        const htmlReadyToServe = this.applyToTemplate(appHtml, preloadLinks);

        res.status(200).set({"Content-Type":"text/html"}).send(htmlReadyToServe);
    }


    private applyToTemplate(appHtml: string, preloadLinks: any) {
        return this.htmlTemplate
            .replace("<!--preload-links-->", preloadLinks)
            .replace("<!--app-html-->", appHtml);
    }
}
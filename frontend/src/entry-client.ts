import {createVueApp} from "./main"

/**
 * ! Select the build type here.
 * ! During "regular" development, this should be set to "false".
 * ! When building for SSR/Hybrid, this has to be set to "true".
 * 
 * ! The SSR Build WILL throw errors/warnings regarding Hydration when running from the dev server.
 * ! This is because an empty HTML-Template is served (the Dev Server does no populate the index.html Template)
 * 
 * TODO: Externalize this into a build flag.
 */

const useSSR = false;

console.info("Using SSR?: "+useSSR);

const {app, router} = createVueApp(!useSSR);

router.isReady().then( () => {
    app.mount("#app");
});
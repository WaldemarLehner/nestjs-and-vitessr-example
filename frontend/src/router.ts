import {createMemoryHistory, createRouter, createWebHistory, RouteRecordRaw} from "vue-router";
import App from "./App.vue";

const routes: RouteRecordRaw[] = [
    {
        path: "/",
        name: "Root",
        component: App
    }
]

const router = () => {

    // Decide which Router to use. Server uses Memory, client uses Web
    const historyImpl = (import.meta.env.SSR) ? createMemoryHistory() : createWebHistory();

    return createRouter({
        history: historyImpl,
        routes
    });

}


export default router;
import { createApp, createSSRApp } from 'vue'
import App from './App.vue'
import createRouter from './router';

export function createVueApp(disableSSR = false)
{
    const router = createRouter();
    const app = disableSSR ?  createApp(App) : createSSRApp(App);
    app.use(router)
    // Add plugins (router, etc) here
    return {
        app,
        router
        /* return router, etc here*/ 
    };
}
